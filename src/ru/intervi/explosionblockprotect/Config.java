package ru.intervi.explosionblockprotect;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.List;

public class Config {
    private final Main MAIN;

    public Config(Main main) {
        MAIN = main;
        load();
    }

    public boolean entityWhitelist;
    public boolean blockWhitelist;
    public List<Material> entityList;
    public List<Material> blockList;

    public void load() {
        MAIN.saveDefaultConfig();
        MAIN.reloadConfig();
        FileConfiguration config = MAIN.getConfig();
        entityWhitelist = config.getBoolean("entityWhitelist");
        blockWhitelist = config.getBoolean("blockWhitelist");
        ArrayList<Material> elist = new ArrayList<>();
        ArrayList<Material> blist = new ArrayList<>();
        for (String str : config.getStringList("entityList")) {
            elist.add(Material.valueOf(str.toUpperCase()));
        }
        for (String str : config.getStringList("blockList")) {
            blist.add(Material.valueOf(str.toUpperCase()));
        }
        entityList = elist;
        blockList = blist;
    }
}
