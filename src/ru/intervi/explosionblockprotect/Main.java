package ru.intervi.explosionblockprotect;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Iterator;

public class Main extends JavaPlugin implements Listener {
    private final Config CONFIG = new Config(this);

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onEntityExplosion(EntityExplodeEvent event) {
        Iterator<Block> iter = event.blockList().iterator();
        while (iter.hasNext()) {
            Material type = iter.next().getType();
            if (CONFIG.entityWhitelist && !CONFIG.entityList.contains(type)) iter.remove();
            else if (!CONFIG.entityWhitelist && CONFIG.entityList.contains(type)) iter.remove();
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockExplosion(BlockExplodeEvent event) {
        Iterator<Block> iter = event.blockList().iterator();
        while (iter.hasNext()) {
            Material type = iter.next().getType();
            if (CONFIG.blockWhitelist && !CONFIG.blockList.contains(type)) iter.remove();
            else if (!CONFIG.blockWhitelist && CONFIG.blockList.contains(type)) iter.remove();
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender.hasPermission("exbp.reload")) {
            CONFIG.load();
            sender.sendMessage("[ExplosionBlocksProtect] config reloaded");
        } else sender.sendMessage("no permission");
        return true;
    }
}
